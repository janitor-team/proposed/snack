This package was debianized by David A. van Leeuwen <vleeuwen@debian.org> on
Thu, 23 Sep 1999 15:24:33 +0200.

It was originally downloaded from http://www.speech.kth.se/snack

Currently, a modification from
http://wize.cvs.sourceforge.net/viewvc/wize/wize2/wize/snack2.2.10/
is used.

The files demos/tcl/tclkit-linux-x86, demos/tcl/sdx,
demos/tcl/right_back.shape and mac/snack.mcp.sit.hqx have been removed
from the upstream tarball because they lack source code.

The files generic/SnackMp3.c, generic/jkFormatMP3.c, generic/jkFormatMP3.h
have been removed from the upstream tarball because they are
not DFSG-free (have restrictions on commercial use).

-----------------------------------------------------------------------
Files: *

Copyright (c) 1997-2005 Jonas Beskow and Kåre Sjölander

Distributed under the GNU general public license, see
/usr/share/common-licenses/GPL

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301 USA

-----------------------------------------------------------------------
Files: generic/jkFormant.h, generic/jkFormant.c

Copyright (c) 1987-1990  AT&T, Inc.
Copyright (c) 1986-1990  Entropic Speech, Inc.
Copyright (c) 1990-1991  Entropic Research Laboratory, Inc.

Distributed under the BSD license:

The following terms apply to all files associated
with the software unless explicitly disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute,
and license this software and its documentation for any purpose, provided
that existing copyright notices are retained in all copies and that this
notice is included verbatim in any distributions. No written agreement,
license, or royalty fee is required for any of the authorized uses.
Modifications to this software may be copyrighted by their authors
and need not follow the licensing terms described here, provided that
the new terms are clearly indicated on the first page of each file where
they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
MODIFICATIONS.

-----------------------------------------------------------------------
Files: generic/sigproc.c

Copyright (c) 1990-1996  Entropic Research Laboratory, Inc.

Distributed under the BSD license:

The following terms apply to all files associated
with the software unless explicitly disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute,
and license this software and its documentation for any purpose, provided
that existing copyright notices are retained in all copies and that this
notice is included verbatim in any distributions. No written agreement,
license, or royalty fee is required for any of the authorized uses.
Modifications to this software may be copyrighted by their authors
and need not follow the licensing terms described here, provided that
the new terms are clearly indicated on the first page of each file where
they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
MODIFICATIONS.

-----------------------------------------------------------------------
Files: generic/SnackMpg.c

Copyright (c) 2009 by Peter MacDonald

Distributed under the BSD license:

The following terms apply to all files associated
with the software unless explicitly disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute,
and license this software and its documentation for any purpose, provided
that existing copyright notices are retained in all copies and that this
notice is included verbatim in any distributions. No written agreement,
license, or royalty fee is required for any of the authorized uses.
Modifications to this software may be copyrighted by their authors
and need not follow the licensing terms described here, provided that
the new terms are clearly indicated on the first page of each file where
they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
MODIFICATIONS.

-----------------------------------------------------------------------
Files: generic/SnackOgg.c

Copyright (c) 1994-2001 by the XIPHOPHORUS Company

This file is combined from vorbisfile.h and vorbisfile.c from
libvorbis library. The original files are distributed under the
BSD license (the license is not included into the source package
and taken from libvorbis source):

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither the name of the Xiph.org Foundation nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------
Files: unix/snack.decls, generic/snackDecls.h, generic/snackStubLib.c,
 generic/snackStubInit.c

Copyright (c) 1998-1999 by Scriptics Corporation

Distributed under the same license as Tcl/Tk itself (the license is not
included into the source package).

This software is copyrighted by the Regents of the University of
California, Sun Microsystems, Inc., Scriptics Corporation,
and other parties.  The following terms apply to all files associated
with the software unless explicitly disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute,
and license this software and its documentation for any purpose, provided
that existing copyright notices are retained in all copies and that this
notice is included verbatim in any distributions. No written agreement,
license, or royalty fee is required for any of the authorized uses.
Modifications to this software may be copyrighted by their authors
and need not follow the licensing terms described here, provided that
the new terms are clearly indicated on the first page of each file where
they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
MODIFICATIONS.

GOVERNMENT USE: If you are acquiring this software on behalf of the
U.S. government, the Government shall have only "Restricted Rights"
in the software and related documentation as defined in the Federal 
Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
are acquiring the software on behalf of the Department of Defense, the
software shall be classified as "Commercial Computer Software" and the
Government shall have only "Restricted Rights" as defined in Clause
252.227-7013 (b) (3) of DFARs.  Notwithstanding the foregoing, the
authors grant the U.S. Government and others acting in its behalf
permission to use and distribute the software in accordance with the
terms specified in this license. 

-----------------------------------------------------------------------
Files: mac/MW_SnackHeaders.pch

Copyright (c) 1995-1997 Sun Microsystems, Inc.

Distributed under the same license as Tcl/Tk itself (the license is not
included into the source package).

-----------------------------------------------------------------------
Files: generic/shape.c

Copyright (c) 2000 Claude Barras

Distributed under the GNU general public licence, though it's unclear
from the package itself. It's labeled as a contribution to the Snack Sound
Toolkit, so it's safe to assume that it's under the same license as the
package itself.

-----------------------------------------------------------------------
Files: generic/ffa.c

Copyright (c) 1995 Roger Lindell

Distributed under the GNU general public licence, though it's unclear
from the package itself. Here is a letter from the author to a
Fedora legal team:

Subject: RE: Looking for the Roger Lindell who wrote "ffa.c"
From: Roger Lindell <Roger.Lindell@proact.se>
Date: 02/13/2013 02:09 AM
To: Tom Callaway <tcallawa@redhat.com>

Hello Tom,

You found the correct Roger Lindell. I am the original author of ffa.c but
it was a port of FORTRAN code from an old IEEE book on Digital Signal
Processing so I'm not quite sure if I'm the copyright holder or not. If
I am the copyright holder then you are free to use it under GPL.

Best regards,

Roger

-----Original Message-----
From: Tom Callaway [mailto:tcallawa@redhat.com] 
Sent: den 12 februari 2013 21:39
To: Roger Lindell
Subject: Looking for the Roger Lindell who wrote "ffa.c"

Hello,

I apologize if I have contacted the wrong Roger Lindell, but I am
looking for the copyright holder (and author) of "ffa.c", a source file
included with the Snack audio utility for TCL.

If this is not you, sorry to have wasted your time! However, if it is
you, fantastic! We are trying to determine the license terms for this
piece of source code, and no license is indicated in the file.

We think it is very likely that this code is under the same license as
the rest of Snack, which is the GNU General Public License (GPL) v2 or
later, but we are hoping you can confirm this.

Thanks in advance,

Tom Callaway
Fedora Legal

==
Fedora Project
